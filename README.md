<h1 align="center">Popular-Linux-Fonts-Installer</h1>

<p align="center">Easily install popular Linux fonts. </p>

### Fonts provided by this repository

* 3270
* AnonymousPro
* AppleEmoji
* CodeNewRoman
* DejaVuSansMono
* DroidSansMono
* FiraCode
* FiraMono
* Hack
* Hellvetica
* Hermit
* InconsolataGo
* InconsolataLGC
* Inconsolata
* Iosevka
* JetBrainsMono
* Liberation
* Monoid
* Noto
* RobotoMono
* RobotoSlab
* SourceCodePro
* Unifont
* Windows


<br/>
